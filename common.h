/* ************************************************************************** */
/** common.h

  @Author
    Galen Nare

  @File Name
    common.h

  @Summary
     Common header for project 2
 */
/* ************************************************************************** */

#ifndef _COMMON_HEADER    /* Guard against multiple inclusion */
#define _COMMON_HEADER

    /*------------------ Board system settings. PLEASE DO NOT MODIFY THIS PART ----------*/
    #pragma config FPLLIDIV = DIV_2         // PLL Input Divider (2x Divider)
    #pragma config FPLLMUL = MUL_20         // PLL Multiplier (20x Multiplier)
    #pragma config FPLLODIV = DIV_1         // System PLL Output Clock Divider (PLL Divide by 1)
    #pragma config FNOSC = PRIPLL           // Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
    #pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disabled)
    #pragma config POSCMOD = XT             // Primary Oscillator Configuration (XT osc mode)
    #pragma config FPBDIV = DIV_8           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/8)
    /*----------------------------------------------------------------------------*/

    #include <stdlib.h>
    #include <stdio.h>
    #include <stdint.h>
    #include <xc.h>     // Microchip XC processor header which links to the PIC32MX370512L header
    #include <sys/attribs.h> 

    #include "config.h" // Basys MX3 configuration header
    #include "acl.h"
    #include "lcd.h"
    #include "ssd.h"

    #define SYS_FREQ (80000000L) // 80MHz system clock
    #define MS_CONSTANT 1426

    #define DET_NUMBER 42

    void delay_ms(int);        // Function prototypes
    
    void lcd_display_start();
    void lcd_display_mode2();
    void lcd_display_mode3(int);
    void lcd_display_feedback(int);
    void lcd_print(char*);
    
    void ssd_write_ints(uint8_t, uint8_t);
    void ssd_write_hex(uint_least32_t);
    void ssd_digit_mask(uint8_t);
    
    void led_active_player(int);
    
    int acl_srand();
    
    void CN_config();
    void CN_button_press(unsigned int);
    
    void Timer2_Setup();
    
#endif /* _COMMON_HEADER */

#ifndef _SUPPRESS_PLIB_WARNING          // suppress the plib warning during compiling
        #define _SUPPRESS_PLIB_WARNING      
#endif

#ifndef _KEYPAD_CONSTANTS
#define _KEYPAD_CONSTANTS
    
    #define R4 LATCbits.LATC14
    #define R3 LATDbits.LATD0
    #define R2 LATDbits.LATD1
    #define R1 LATCbits.LATC13

    #define C4 PORTDbits.RD9
    #define C3 PORTDbits.RD11
    #define C2 PORTDbits.RD10
    #define C1 PORTDbits.RD8

#endif
    
/* *****************************************************************************
 End of File
 */
